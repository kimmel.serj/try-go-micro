# TryGoMicro Service

This is the TryGoMicro service

Generated with

```
micro new try-go-micro
```

## Usage

Generate the proto code

```
make proto
```

Run the service

```
micro run .
```