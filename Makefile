
GOPATH:=$(shell go env GOPATH)
SERVICE_VERSION ?= $(or $(CI_COMMIT_REF_NAME), "dev")

.PHONY: init
init:
	go get -u github.com/golang/protobuf/proto
	go get -u github.com/golang/protobuf/protoc-gen-go
	go get github.com/micro/micro/v3/cmd/protoc-gen-micro
	go get github.com/micro/micro/v3/cmd/protoc-gen-openapi

.PHONY: api
api:
	protoc --openapi_out=. --proto_path=. proto/try-go-micro.proto

.PHONY: proto
proto:
	protoc --proto_path=. --micro_out=. --go_out=:. proto/try-go-micro.proto
	
.PHONY: build
build:
	go build -race -ldflags "-extldflags '-static' -X main.ServiceVersion=$(SERVICE_VERSION)" -o try-go-micro

.PHONY: lint
lint:
	go vet ./...

.PHONY: test
test:
	go test -cover -race -v ./...
