package main

import (
	"try-go-micro/handler"
	pb "try-go-micro/proto"

	"github.com/micro/micro/v3/service"
	"github.com/micro/micro/v3/service/logger"
)

const (
	ServiceName = "try-go-micro"
)

var (
	ServiceVersion string = "dev"
)

func main() {
	// Create service
	srv := service.New(
		service.Name(ServiceName),
		service.Version(ServiceVersion),
	)

	// Register handler
	pb.RegisterTryGoMicroHandler(srv.Server(), new(handler.TryGoMicro))

	// Run service
	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
