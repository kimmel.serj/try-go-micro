FROM alpine:3.14.1
ADD try-go-micro /try-go-micro
ENTRYPOINT [ "/try-go-micro" ]
