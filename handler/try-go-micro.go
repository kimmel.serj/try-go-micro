package handler

import (
	"context"

	log "github.com/micro/micro/v3/service/logger"

	trygomicro "try-go-micro/proto"
)

type TryGoMicro struct{}

// Call is a single request handler called via client.Call or the generated client code
func (e *TryGoMicro) Call(ctx context.Context, req *trygomicro.Request, rsp *trygomicro.Response) error {
	log.Info("Received TryGoMicro.Call request")
	rsp.Msg = "Hello " + req.Name
	return nil
}

// Stream is a server side stream handler called via client.Stream or the generated client code
func (e *TryGoMicro) Stream(ctx context.Context, req *trygomicro.StreamingRequest, stream trygomicro.TryGoMicro_StreamStream) error {
	log.Infof("Received TryGoMicro.Stream request with count: %d", req.Count)

	for i := 0; i < int(req.Count); i++ {
		log.Infof("Responding: %d", i)
		if err := stream.Send(&trygomicro.StreamingResponse{
			Count: int64(i),
		}); err != nil {
			return err
		}
	}

	return nil
}

// PingPong is a bidirectional stream handler called via client.Stream or the generated client code
func (e *TryGoMicro) PingPong(ctx context.Context, stream trygomicro.TryGoMicro_PingPongStream) error {
	for {
		req, err := stream.Recv()
		if err != nil {
			return err
		}
		log.Infof("Got ping %v", req.Stroke)
		if err := stream.Send(&trygomicro.Pong{Stroke: req.Stroke}); err != nil {
			return err
		}
	}
}
